CREATE TABLE bookdb.book (
  id INT NOT NULL,
  title VARCHAR(45) NULL,
  genre VARCHAR(45) NULL,
  PRIMARY KEY (id));