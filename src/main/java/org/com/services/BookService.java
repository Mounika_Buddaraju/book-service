package org.com.services;
import java.util.List;
import org.com.entities.Book;
import org.com.exception.BookException;
import org.com.repository.BookRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class BookService {
	@Autowired
	BookRepository bookRepository;

	public List<Book> getAllBooks() {
		return bookRepository.findAll();
	}

	public Book getBookById(int id) throws BookException {
		return bookRepository.findById(id).orElseThrow(() -> new BookException("BOOK NOT FOUND WITH ID :" + id));
	}

	public Book createBook(Book book) {
		return bookRepository.save(book);
	}

	public void deleteBook(int id) throws BookException {
		Book book = bookRepository.findById(id).orElseThrow(() -> new BookException("BOOK NOT FOUND WITH ID :" + id));
		bookRepository.delete(book);
	}
	
	public Book updateBook(int id,Book book) throws BookException {
		if (id != book.getId())
            throw new BookException("BOOK ID MISMATCH....YOU CANNOT CHANGE ID :" + id);
		bookRepository.save(book);
		return book;
	}

}
