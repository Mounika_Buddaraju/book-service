package org.com.restcontrollers;
import javax.servlet.http.HttpServletRequest;
import org.com.entities.Book;
import org.com.exception.BookException;
import org.com.exception.BookExceptionResponse;
import org.com.services.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class BookRestController {
	
	@Autowired
	BookService bookSevice;
	
	@GetMapping("/books")
	public ResponseEntity<?> getAllBooks(){
		return new ResponseEntity<>(bookSevice.getAllBooks(),HttpStatus.OK);
	}
	
	@GetMapping("/books/{id}")
	public ResponseEntity<?> getBookById(@PathVariable int id ,final HttpServletRequest request) {
		try {
		return new ResponseEntity<>(bookSevice.getBookById(id),HttpStatus.OK);
		}catch(BookException exception) {
			return new ResponseEntity<>(new BookExceptionResponse(exception.getMessage(), request.getRequestURI()),
					HttpStatus.NOT_FOUND);
		}
	}
	
	@PostMapping("/books")
	public ResponseEntity<?> createBook(@RequestBody Book book){
		return new ResponseEntity<>( bookSevice.createBook(book),HttpStatus.CREATED);
	}
	
	@DeleteMapping("/books/{id}")
	public ResponseEntity<?> deleteBook(@PathVariable int id,final HttpServletRequest request) {
		try {
			bookSevice.deleteBook(id);
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}catch(BookException exception) {
			return new ResponseEntity<>(new BookExceptionResponse(exception.getMessage(), request.getRequestURI()),
					HttpStatus.NOT_FOUND);
		}
	}
	
	@PutMapping("/books/{id}")
	public ResponseEntity<?> updateBook(@PathVariable int id, @RequestBody Book book,final HttpServletRequest request) {
		try {
			return new ResponseEntity<>(bookSevice.updateBook(id,book),HttpStatus.OK);
		} catch (BookException exception) {
			return new ResponseEntity<>(new BookExceptionResponse(exception.getMessage(), request.getRequestURI()),
					HttpStatus.NOT_FOUND);
		}
	}
}
		
