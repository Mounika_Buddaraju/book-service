package org.com.services;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.when;
import static org.assertj.core.api.Assertions.assertThatCode;
import static org.junit.jupiter.api.Assertions.assertTrue;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.com.entities.Book;
import org.com.exception.BookException;
import org.com.repository.BookRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.data.repository.CrudRepository;

public class BookServiceTest {

	@Mock
	BookRepository bookRepository;

	@InjectMocks
	BookService bookService;

	@BeforeEach
	public void init() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void getAllBooksTest() {
		List<Book> booksList=new ArrayList<>();
		booksList.add(new Book(1,"The DaVinci Code","Thriller"));
		when(bookRepository.findAll()).thenReturn(booksList);
		List<Book> actualList = bookService.getAllBooks();
		for (int i = 0; i < booksList.size(); i++) {
			assertTrue(booksList.get(i).equals(actualList.get(i)));
		} 
	}

	@Test
	public void getBookByIdTest() throws BookException 
	{
		Book book = new Book(1,"The DaVinci Code","Thriller");
		Optional<Book> expectedBook = Optional.of(book);
		when(bookRepository.findById(book.getId())).thenReturn(expectedBook);
		Book actualBook =bookService.getBookById(1);
		assertTrue(actualBook.getTitle().equals(book.getTitle()));
	}
	
	@Test
    public void getBookByIdIfNOBookTest() {
        given(this.bookRepository.findById(1)).willReturn(Optional.empty());
        assertThatCode(() -> bookService.getBookById(22)).isInstanceOf(BookException.class)
                .hasMessageContaining("BOOK NOT FOUND WITH ID :22");
    }

	
	@Test
	public void createBookTest()
	{
		Book book = new Book(1,"The DaVinci Code","Thriller");
		when(bookRepository.save(Mockito.any())).thenReturn(book);
		assertTrue(bookService.createBook(book).getTitle().equals(book.getTitle()));

	}

	@Test
	public void updateBookTest() throws BookException
	{
		Book expectedBook = new Book(1,"The DaVinci Code","Thriller");
		when(bookRepository.save(Mockito.any())).thenReturn(expectedBook);
		Book actualBook = bookService.updateBook(1,expectedBook);
		assertTrue(actualBook.getTitle().equals(expectedBook.getTitle()));

	}
	
	 @Test
	  public void updateBookIfNoBookTest() {
	        given(this.bookRepository.findById(1)).willReturn(Optional.empty());
	        assertThatCode(() -> bookService.updateBook(22, new Book())).isInstanceOf(BookException.class)
	                .hasMessageContaining("BOOK ID MISMATCH....YOU CANNOT CHANGE ID :22");
	    }
	
	@Test
	public void deleteBookTest() throws BookException {
		Book book = new Book(1,"The DaVinci Code","Thriller");
		Optional<Book> expectedBook = Optional.of(book);
		when(bookRepository.findById(book.getId())).thenReturn(expectedBook);
		bookService.deleteBook(1);	
	}
	
	@Test
	public void deleteBookIfNoBookTest() throws BookException {
		 given(this.bookRepository.findById(1)).willReturn(Optional.empty());
	        assertThatCode(() -> bookService.deleteBook(22)).isInstanceOf(BookException.class)
	                .hasMessageContaining("BOOK NOT FOUND WITH ID :22");
	}
}
