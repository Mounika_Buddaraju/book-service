package org.com.controller;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import java.util.ArrayList;
import java.util.List;
import org.com.entities.Book;
import org.com.restcontrollers.BookRestController;
import org.com.services.BookService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatchers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import com.fasterxml.jackson.databind.ObjectMapper;

@WebMvcTest
class ControllerTest {
   @Autowired
   private MockMvc mockMvc;
   @Autowired
   BookRestController bookController;
   @MockBean
   BookService bookService;

   List<Book> bookList;
   Book book;
   
   @BeforeEach
   public void setup() {
       bookList = new ArrayList<>();
       bookList.add(new Book(1, "title", "genre"));
       bookList.add(new Book(2, "title2", "genre2"));
   }

   @Test
   public void testGetAllBooks() throws Exception {
       when(bookService.getAllBooks()).thenReturn(bookList);
       this.mockMvc.perform(get("/books")).andExpect(status().isOk());
   }

   @Test
   public void testGetBookById() throws Exception {
       when(bookService.getBookById(1)).thenReturn(book);
       this.mockMvc.perform(get("/books/1").param("id", "1")).andExpect(status().isOk());
   }

   @Test
   public void testDeleteBook() throws Exception {
       doNothing().when(bookService).deleteBook(1);
       this.mockMvc.perform(delete("/books/1").param("id", "1")).andExpect(status().isNoContent());
   }

   @Test
   public void testCreateBook() throws Exception {
       when(bookService.createBook(ArgumentMatchers.any())).thenReturn(book);
       mockMvc.perform(post("/books").contentType("application/json")
               .content(asJsonString(new Book(2, "title2", "genre1")))).andExpect(status().isCreated());
   }


   @Test
   public void testUpdateBook() throws Exception {
       when(bookService.updateBook(1,book)).thenReturn(book);
       this.mockMvc.perform(put("/books/1").param("id", "1").contentType("application/json").content(asJsonString(new Book(1, "title2", "genre1"))))
               .andExpect(status().isOk());
   }


   public static String asJsonString(final Object obj) {
       try {
           return new ObjectMapper().writeValueAsString(obj);
       } catch (Exception e) {
           throw new RuntimeException(e);
       }
   }
}